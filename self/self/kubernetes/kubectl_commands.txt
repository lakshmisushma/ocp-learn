kubectl config set-credentials kubernetes-admin/foo.kubernetes.com --username=kubernetes-admin --token=eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZC10b2tlbi1nN2Q1NSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjM3OThmMzVkLTNiM2ItMTFlOS1hNWQ5LTAyNGI4OGVmNmU0ZSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDprdWJlLXN5c3RlbTprdWJlcm5ldGVzLWRhc2hib2FyZCJ9.dL1k6wizD7H2mW67NhOAc5JiNtHqCDqkXO53ghCoqyn0C8bEIpbRo7twIW8QVM9_1CzQkkplmhO-IrmamSgunWmpw_PDtSf4uKZXqeLUNjBoQzxwXepDXWssRThR9JE2BDG02JCCI7ksHoMD6-fXXttmgt0cwV-Vb7Eg9a9YJG35QrBrAFOziHDSTeUcRLBhXV_RGuompJk65QwqUqtW4w6PUFooPHr_gYWJ4x-JpJG5BBKcWp9K1BdshcE8ZZMWjkhy1ma1w5HtI1HwOmBGdCrDkD5nrIB4WSs8w0TC42CqyRmp7HiKoB_FpP0Exuy9i6o8KG-z9E-CiInvr31_qA

username:  kubernetes-admin

kubectl config set-cluster https://ec2-13-58-63-129.us-east-2.compute.amazonaws.com:31675 --insecure-skip-tls-verify=true --server=https://ec2-13-58-63-129.us-east-2.compute.amazonaws.com:31675

kubectl config set-context <projectname>/https://ec2-13-58-63-129.us-east-2.compute.amazonaws.com:31675/kubernetes-admin --user=kubernetes-admin/https://ec2-13-58-63-129.us-east-2.compute.amazonaws.com:31675 --namespace=<your namespace> --cluster=https://ec2-13-58-63-129.us-east-2.compute.amazonaws.com:31675

kubectl config use-context <projectname>/https://ec2-13-58-63-129.us-east-2.compute.amazonaws.com:31675/kubernetes-admin


node {
  stage('Apply Kubernetes files') {
    withKubeConfig([credentialsId: 'a32b4494-274a-44d7-92f1-a70102c18ee4', serverUrl: 'https://api.k8s.dev.xlabsdfs.com']) {
      sh 'kubectl get nodes'
    }
  }
} 


      imagePullSecrets:
      - name: dockerregistrysecret