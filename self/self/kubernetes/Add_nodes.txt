To add nodes for the cluster following are the steps:
  
 hostnamectl set-hostname MUFG-DEV-MS-Kube-Node1
    2  vim /etc/hosts
    3  reboot
    4  apt-get update
    5  apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
    6  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    7  add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    8     $(lsb_release -cs) \
    9     stable"
   10  apt-get update
   11  apt-cache madison docker-ce
   12  apt-get install -y docker-ce=17.03.0~ce-0~ubuntu-xenial containerd.io
   13  docker --version
   14  cat << EOF > /etc/docker/daemon.json
   15  {
   16  "exec-opts": ["native.cgroupdriver=systemd"]
   17  }
   18  EOF
   19  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
   20  cat << EOF > /etc/apt/sources.list.d/kubernetes.list
   21  deb http://apt.kubernetes.io/ kubernetes-xenial main
   22  EOF
   23  apt update -y
   24  apt install -y kubelet=1.10.13-00 kubeadm=1.10.13-00 kubectl=1.10.13-00
   25  kubeadm join 10.0.0.6:6443 --token mqhkcx.rlu79j89g07fyj0j --discovery-token-ca-cert-hash sha256:00de6eefd64a95c3001644e4d6cac394d4088e60a339ce7d95af0aab5536321b
   26  wget https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.0.0-beta.2/crictl-1.0.0-beta.2-linux-amd64.tar.gz
   27  sudo tar zxvf crictl-1.0.0-beta.2-linux-amd64.tar.gz -C /usr/local/bin